c:\>cd xampp/mysql/bin
c:\xampp\mysql\bin>mysql -uroot

1. Membuat database
create database myshop;

2. Membuat table
- categories
  create table categories(id int(8) auto_increment, name varchar(30), primary key(id));
- items
  create table items (id int(8) auto_increment, name varchar(255), description varchar(255), price int(8), stock int(8),category_id int(8), primary key(id), foreign key(category_id) references categories(id));
- users
  create table users (id int(8) auto_increment, name varchar(255), email varchar(255), password varchar(255), primary key(id));

3. Memasukan data pada table
   users:
-  insert into users (name, email, password)
    -> values ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jane123");
   
   categories:
-  insert into categories (name)
    -> values ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

   items:
-  insert into items (name, description, price, stock, category_id)
    -> values ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil data dari database
 a. users data tanpa password
    select id, name, email from users;

 b. Mengambil data items
    -Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
     select * from items where price > 1000000;

    -Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
     select * from items where name like '%sang%';

 c. Menampilkan data items join dengan categories
    select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;

5. Mengganti harga
   update items set price = 2500000 where id = 1;





